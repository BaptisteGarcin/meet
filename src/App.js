// Import FirebaseAuth and firebase.
import React from "react";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import firebase from "firebase";
import "firebase/firestore";



/**
 * Sample JavaScript code for youtube.subscriptions.list
 * See instructions for running APIs Explorer code samples locally:
 * https://developers.google.com/explorer-help/guides/code_samples#javascript
 */

function authenticate() {
    return window.gapi.auth2.getAuthInstance()
        .signIn({scope: "https://www.googleapis.com/auth/youtube.readonly"})
        .then(function () {
                console.log("Sign-in successful");
            },
            function (err) {
                console.error("Error signing in", err);
            });
}

function loadClient() {
    window.gapi.client.setApiKey("AIzaSyD_Be9fbVJ53PlKZpXSKO4FK8FJxKN1T1Y");
    return window.gapi.client.load("https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest")
        .then(function () {
                console.log("GAPI client loaded for API");
                execute()
            },
            function (err) {
                console.error("Error loading GAPI client for API", err);
            });
}

// Make sure the client is loaded and sign-in is complete before calling this method.
function execute() {
    console.log(window.gapi.auth2.getAuthInstance().isSignedIn.get());

    return window.gapi.client.youtube.subscriptions.list({
        part: "snippet",
        mine: true,
        maxResults: 50
    })
        .then(function (response) {
                // Handle the results here (response.result has the parsed body).
                console.log("Response", response);
            },
            function (err) {
                console.error("Execute error", err);
            });
}

window.gapi.load("client:auth2", initClient);
// Configure Firebase.
const config = {
  apiKey: "AIzaSyD_Be9fbVJ53PlKZpXSKO4FK8FJxKN1T1Y",
  authDomain: "meet2-ffdaf.firebaseapp.com",
  databaseURL: "https://meet2-ffdaf.firebaseio.com",
  projectId: "meet2-ffdaf",
  storageBucket: "",
  messagingSenderId: "424398156280",
  appId: "1:424398156280:web:acc1204a187a8b18",
  clientId:
    "424398156280-ia1vh1s44b7qqvm5kk61a5gako72kvb9.apps.googleusercontent.com",
  scopes: [
    "email",
    "profile",
    "openid",
    "https://www.googleapis.com/auth/youtube.readonly"
  ],
  discoveryDocs: [
    "https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest"
  ]
};

firebase.initializeApp(config);
const db = firebase.firestore();

// Configure FirebaseUI.
const uiConfig = {
  // Popup signin flow rather than redirect flow.
  signInFlow: "popup",
  // Redirect to /signedIn after sign in is successful. Alternatively you can provide a callbacks.signInSuccess function.
  //signInSuccessUrl: "/signedIn",
  // We will display Google and Facebook as auth providers.
  signInOptions: [
    {
      provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      scopes: config.scopes
    }
  ],
  callbacks: {
    signInSuccessWithAuthResult: () => false
  }
};

function get_subscriptions(token) {
  return new Promise(function(resolve, reject) {
    const rq = {
      part: "snippet",
      mine: true,
      maxResults: 50
    };
    if (token) {
      // If we got a token from previous call
      rq.pageToken = token; // .. attach it to the new request
    }
    const request = window.gapi.client.youtube.subscriptions.list(rq);

    request.execute(function(response) {
      resolve(response.result);
    });
  });
}

async function getAllSubscriptions() {
  let subscriptions = [];
  let res = await get_subscriptions();
  console.log(res);
  subscriptions.push(res.items);
  while (res.nextPageToken) {
    res = await get_subscriptions(res.nextPageToken);
    subscriptions.push(res.items);
  }
  return subscriptions.flat();
}

// Make sure the client is loaded and sign-in is complete before calling this method.
function findSubscriptionsSimilarities() {
  return getAllSubscriptions()
    .then(res => {
      return res.map(item => {
        return {
          subscription: {
            channel: item.snippet.title,
            id: item.snippet.resourceId.channelId
          }
        };
      });
    })
    .then(subs => {
        db.collection("users")
            .doc(firebase.auth().currentUser.uid)
            .set({
                //Add sex ?
                subscriptions: subs
            });

        //addMockDataToFirebase(5, subs);
        let subsSimilarities = [];
        //Get all subscriptions from firestore
        return db
            .collection("users")
            .get()
            .then(snapshot => {
                snapshot.forEach(doc => {
                    let ctr = 0;
                    const commonSubs = subs.map(item => {
                        return doc.data().subscriptions.map(item2 => {
                            if (item2.subscription.id === item.subscription.id){
                                ctr++;
                                return item2.subscription;
                            }
                        }).filter(item3 => item3);
                    }).flat();
                    subsSimilarities.push({
                        commonSubs: commonSubs,
                        similarity: (doc.data().subscriptions.length / ctr) * 100,
                        checkedWithUser: doc.id,
                        originUser: firebase.auth().currentUser.uid
                    });
                });
            })
            .catch(err => {
                console.log("Error getting documents", err);
            })
            .then(fin => {
                console.log("arrayPercentages", subsSimilarities);
                db.collection("users")
                    .doc(firebase.auth().currentUser.uid)
                    .update({
                        //Add sex ?
                        subsSimilarities: subsSimilarities
                    });
                return subsSimilarities;
            });
    });
}

// Initialize the API client library
function initClient() {
  window.gapi.client.init({
    apiKey: config.apiKey,
    discoveryDocs: config.discoveryDocs,
    clientId: config.clientId,
    scope: "https://www.googleapis.com/auth/youtube.readonly"
  }).then(() => console.log("fclient loaded"))
}

async function login(){
    const googleAuth = window.gapi.auth2.getAuthInstance()
    const googleUser = await googleAuth.signIn({
      ux_mode: 'popup'
    });

    const token = googleUser.getAuthResponse().id_token;

    const credential = firebase.auth.GoogleAuthProvider.credential(token);
    return await firebase.auth().signInWithCredential(credential);
}

function guidGenerator() {
    /**
     * @return {string}
     */
    let S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}

function addMockDataToFirebase(nbMocks, subs){
    while (nbMocks > 0){
        db.collection("users")
            .doc(guidGenerator())
            .set({
                //Add sex ?
                subscriptions: subs.slice(Math.floor(Math.random() * subs.length), Math.floor(Math.random() * subs.length))
            });
            nbMocks--;
    }
}

class App extends React.Component {
  state = {
    isSignedIn: false,
    subsSimilarities: []
  };

  componentDidMount() {
    this.unregisterAuthObserver = firebase.auth().onAuthStateChanged(user => {
      if (user) {
        //making sure gapi is loaded
            console.log("test")
            findSubscriptionsSimilarities().then(subsSimilarities => 
                this.setState({subsSimilarities : subsSimilarities}
            ));
          // Initialize the Google API Client with the config object
                  // Make sure the Google API Client is properly signed in
        this.setState({ isSignedIn: true });
      } else {
        this.setState({ isSignedIn: false });
      }
    });
  }

  componentWillUnmount() {
    this.unregisterAuthObserver();
  }

  render() {
    return (
      <div>
        <h1>Love Solutions</h1>
        {this.state.isSignedIn ? (
          <div>
            You are signed In!
            {this.state.subsSimilarities.length > 0 ? (
                <div>
                {this.state.subsSimilarities.map(obj => {
                  return <div>
                      <p>{obj.similarity.toString() + "% similarity with user : " + obj.checkedWithUser.toString()}</p>
                      {obj.commonSubs.map( arr =>{
                            console.log(arr);
                            return <li>{arr.channel}</li>
                      })}
                  </div>;
                })}
                </div>
            ) : (
              <p>Loading...</p>
            )}
            <button onClick={() => firebase.auth().signOut()}>Sign-out</button>
          </div>
        ) : (
          <div>
              <button >Login</button>
              <button onClick={() => login()} className="btn-google">        
              <span>     
                <img width="20px" alt="Google &quot;G&quot; Logo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png"/> 
                <span className="text-google">Sign in with Google</span>
                </span>
              </button>
          </div>
        )}
      </div>
    );
  }
}

export default App;
